#
# @author  Daniel Maturana
# @year    2015
#
# @attention Copyright (c) 2015
# @attention Carnegie Mellon University
# @attention All rights reserved.
#
# @=



import sys

import matplotlib.pyplot as pl
import numpy as np

pl.clf()
unif01 = np.loadtxt('unif01.txt')
pl.hist(unif01, bins=100, normed=True)
pl.savefig('unif01.png')

pl.clf()
unif_int1 = np.loadtxt('unif_int1.txt')
print 'unif_int1:', unif_int1.min(), unif_int1.max(), unif_int1.mean()
pl.hist(unif_int1, bins=100, normed=True)
pl.savefig('unif_int1.png')

pl.clf()
unif_int2 = np.loadtxt('unif_int2.txt')
print 'unif_int2:', unif_int2.min(), unif_int2.max(), unif_int2.mean()
pl.hist(unif_int2, bins=100, normed=True)
pl.savefig('unif_int2.png')



