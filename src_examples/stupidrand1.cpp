/**
 * @author  Daniel Maturana
 * @year    2015
 *
 * @attention Copyright (c) 2015
 * @attention Carnegie Mellon University
 * @attention All rights reserved.
 *
 **@=*/



#include <iostream>
#include <fstream>

#include "random_util/simple_random.hpp"

int main(int argc, char *argv[]) {

  ca::sr::ClockSeed();

  // should we have tests with some sort of
  // statistical bound/hypothesis test?
  static const int N = 4000;
  {
    std::ofstream out("unif01.txt", std::ios::out);
    for (int i=0; i<N; ++i) {
      float u = ca::sr::Random();
      out << u << "\n";
    }
    out.close();
  }

  {
    std::ofstream out("unif_int1.txt", std::ios::out);
    for (int i=0; i<N; ++i) {
      float u = ca::sr::RandomInt(0, 32);
      out << u << "\n";
    }
    out.close();
  }

  {
    std::ofstream out("unif_int2.txt", std::ios::out);
    for (int i=0; i<N; ++i) {
      float u = ca::sr::RandomInt(-32, 32);
      out << u << "\n";
    }
    out.close();
  }

  return 0;
}
