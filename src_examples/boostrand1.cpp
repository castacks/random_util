/**
 * @author  Daniel Maturana
 * @year    2015
 *
 * @attention Copyright (c) 2015
 * @attention Carnegie Mellon University
 * @attention All rights reserved.
 *
 **@=*/



#include <iostream>
#include "random_util/boost_random.hpp"

int main(int argc, char *argv[]) {
  namespace br = ca::boost_random;

  {
    br::UniformReald gen(-2., 2.);
    gen.ClockSeed();
    for (int i=0; i < 10; ++i) {
      double u = gen();
      std::cout << u << " ";
    }
    std::cout << "\n";
  }

  {
    br::Normald gen(0., 1.);
    gen.ClockSeed();
    for (int i=0; i < 10; ++i) {
      double u = gen();
      std::cout << u << " ";
    }
    std::cout << "\n";
  }

  return 0;
}
