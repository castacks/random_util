/**
 * @author  Daniel Maturana
 * @year    2015
 *
 * @attention Copyright (c) 2015
 * @attention Carnegie Mellon University
 * @attention All rights reserved.
 *
 **@=*/



#include <Eigen/Core>

#include <iostream>
#include "random_util/random_points.hpp"

int main(int argc, char *argv[]) {

  ca::UniformVector3d rng;
  rng.ClockSeed();
  for (int i=0; i < 10; ++i) {
    Eigen::Vector3d vec = rng();
    std::cerr << vec.transpose() << std::endl;
  }

  return 0;
}
