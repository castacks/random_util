/**
 * @author  Daniel Maturana
 * @year    2015
 *
 * @attention Copyright (c) 2015
 * @attention Carnegie Mellon University
 * @attention All rights reserved.
 *
 **@=*/


#ifndef BOOSTRAND_HPP_YL2H7IDS
#define BOOSTRAND_HPP_YL2H7IDS

/**
 * A simple wrapper for boost::random becaues its API
 * is hard to remember.
 * Only basic capabilities.
 * For more sophisticated stuff just use the original API
 */
#include <boost/random/mersenne_twister.hpp>
#include <boost/random/uniform_01.hpp>
#include <boost/random/uniform_real.hpp>
#include <boost/random/normal_distribution.hpp>
#include <boost/random/variate_generator.hpp>

namespace ca { namespace boost_random {

unsigned int GetClockSeed() {
  return static_cast<unsigned int>(time(0));
}

/**
 *  semantics of copying/assignment:
 *  memberwise copies mean that a duplicate of the generator
 *  is constructed. this copies the state of the PRNG.
 *  so do it with care.
 */
template<class RealType>
class UniformReal {
private:
 typedef boost::mt19937 RngType;
 typedef typename boost::uniform_real<RealType> DistType;
 typedef boost::variate_generator<RngType, DistType> Gen;

public:
  // Note that there is a specialized uniform_01 generator.
  UniformReal() :
      a_(0), b_(1),
      seed_(0),
      gen_(RngType(seed_), DistType(0, 1) )
  { }

  UniformReal(RealType a, RealType b) :
      a_(a), b_(b),
      seed_(0),
      gen_(RngType(seed_), DistType(a, b) )
  { }

  /**
   * Note that this reinitializes the RngType.
   */
  void Seed(unsigned int s) {
    gen_ = Gen(RngType(s), DistType(a_, b_));
  }

  /**
   * Note that this will only be the same when invoked in the same second.
   */
  void ClockSeed() {
    gen_ = Gen(RngType(static_cast<unsigned int>(time(0))),
               DistType(a_, b_));
  }

  RealType operator()() { return gen_(); }

  virtual ~UniformReal() { }

private:
  RealType a_, b_;
  unsigned int seed_;
  Gen gen_;


};

typedef UniformReal<float> UniformRealf;
typedef UniformReal<double> UniformReald;

template<class RealType>
class Normal {
private:
 typedef boost::mt19937 RngType;
 typedef typename boost::normal_distribution<RealType> DistType;
 typedef boost::variate_generator<RngType, DistType> Gen;

public:
  // Note that there is a specialized uniform_01 generator.
  Normal() :
      mean_(0), std_dev_(1),
      seed_(0),
      gen_(RngType(seed_), DistType(mean_, std_dev_) )
  { }

  Normal(RealType mean, RealType std_dev) :
      mean_(mean), std_dev_(std_dev),
      seed_(0),
      gen_(RngType(seed_), DistType(mean_, std_dev_) )
  { }

  /**
   * Note that this reinitializes the RngType.
   */
  void Seed(unsigned int s) {
    gen_ = Gen(RngType(s), DistType(mean_, std_dev_));
  }

  /**
   * Note that this will only be the same when invoked in the same second.
   */
  void ClockSeed() {
    gen_ = Gen(RngType(static_cast<unsigned int>(time(0))),
               DistType(mean_, std_dev_));
  }

  RealType operator()() { return gen_(); }

  virtual ~Normal() { }

private:
  RealType mean_, std_dev_;
  unsigned int seed_;
  Gen gen_;

private:
  Normal(const Normal& other);
  Normal& operator=(const Normal& other);

};

typedef Normal<float> Normalf;
typedef Normal<double> Normald;

} }/* ca */

#endif /* end of include guard: BOOSTRAND_HPP_YL2H7IDS */
