/**
 * @author  Daniel Maturana
 * @year    2015
 *
 * @attention Copyright (c) 2015
 * @attention Carnegie Mellon University
 * @attention All rights reserved.
 *
 **@=*/


#ifndef RANDOM_POINTS_HPP_XQNR9KOA
#define RANDOM_POINTS_HPP_XQNR9KOA

#include <iostream>
#include <Eigen/Core>

#include <boost/random/mersenne_twister.hpp>
#include <boost/random/uniform_01.hpp>
#include <boost/random/uniform_real.hpp>
#include <boost/random/normal_distribution.hpp>
#include <boost/random/variate_generator.hpp>

namespace ca
{

/**
 * Note that Eigen has a similar setRandom functionality.
 */
template<class RealType, int rows, int cols>
class UniformRealFixedSize {
private:
 typedef boost::mt19937 RngType;
 typedef typename boost::uniform_real<RealType> DistType;
 typedef boost::variate_generator<RngType, DistType> Gen;

public:
  // Note that there is a specialized uniform_01 generator.
  UniformRealFixedSize() :
      a_(0), b_(1),
      seed_(0),
      gen_(RngType(seed_), DistType(0, 1) )
  { }

  UniformRealFixedSize(RealType a, RealType b) :
      a_(a), b_(b),
      seed_(0),
      gen_(RngType(seed_), DistType(a, b) )
  { }

  /**
   * Note that this reinitializes the RngType.
   */
  void Seed(unsigned int s) {
    gen_ = Gen(RngType(s), DistType(a_, b_));
  }

  /**
   * Note that this will only be the same when invoked in the same second.
   */
  void ClockSeed() {
    gen_ = Gen(RngType(static_cast<unsigned int>(time(0))),
               DistType(a_, b_));
  }

  Eigen::Matrix<RealType, rows, cols> operator()() {
    Eigen::Matrix<RealType, rows, cols> vec;
    // can't use std::generate because it copies!
    //std::generate(vec.data(), vec.data()+3, gen_);
    for (RealType* ptr = vec.data();
         ptr != vec.data()+vec.size();
         ++ptr) {
      *ptr = gen_();
    }
    return vec;
  }

  virtual ~UniformRealFixedSize() { }

private:
  RealType a_, b_;
  unsigned int seed_;
  Gen gen_;

};

typedef UniformRealFixedSize<float, 2, 1> UniformVector2f;
typedef UniformRealFixedSize<double, 2, 1> UniformVector2d;
typedef UniformRealFixedSize<float, 3, 1> UniformVector3f;
typedef UniformRealFixedSize<double, 3, 1> UniformVector3d;
typedef UniformRealFixedSize<float, 4, 1> UniformVector4f;
typedef UniformRealFixedSize<double, 4, 1> UniformVector4d;

} /* ca */

#endif /* end of include guard: RANDOM_POINTS_HPP_XQNR9KOA */
