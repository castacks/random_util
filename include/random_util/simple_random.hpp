/**
 * @author  Daniel Maturana
 * @year    2015
 *
 * @attention Copyright (c) 2015
 * @attention Carnegie Mellon University
 * @attention All rights reserved.
 *
 **@=*/


#ifndef STUPIDRAND_HPP_QIVPIOWG
#define STUPIDRAND_HPP_QIVPIOWG

/**
 * Note: not a serious random generator.
 * Don't use for cryptography.
 * Don't use in multithreaded scenarios.
 */

#include <math.h>
#include <float.h>
#include <stdlib.h>
#include <time.h>

namespace ca { namespace sr {

inline void ClockSeed() {
  srand(static_cast<unsigned int>(time(NULL)));
}

inline float Random() {
  return float(rand())/(float(RAND_MAX)+1.0);
}

inline float Uniform(float a, float b) {
  return (b-a)*ca::sr::Random() + a;
}

// return int in range [a,b)
inline int RandomInt(int a, int b) {
  return (static_cast<int>(floor((ca::sr::Random()-FLT_EPSILON)*(b-a)+a)));
}

inline bool Flip(float p) {
  return ca::sr::Random() < p;
}

} }

#endif /* end of include guard: STUPIDRAND_HPP_QIVPIOWG */
